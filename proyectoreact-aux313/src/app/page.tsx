
import Barras from "./componentes/Barras";
import Footer from "./componentes/Footer";
import Header from "./componentes/Header";
import Icono from "./componentes/Icono";
import Parte1 from "./componentes/Parte1";
import Parte2 from "./componentes/Parte2";
export default function Home() {
  return (
    <main >

      <div className="fondodecolor">
        <Header></Header>
        <div className="fondo1">
          <div className="fondo">
            <section className="part1">
              <div className="parte1">
                <Parte1 ></Parte1>
              </div>
              <div className="parte2">
                <img src="/imagenes/imagen1.svg" alt="" width="595px" height="750px" />
              </div>
            </section>
          </div>
        </div>
      </div>
      <div className="bar">
        <Barras icon="/imagenes/bar1.svg" heading="Learn The Latest Skills" text="Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a BC, making it over 2000 years old" ></Barras>
        <Barras icon="/imagenes/bar1.svg" heading="Get Ready For a Career" text="Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a BC, making it over 2000 years old." />
        <Barras icon="/imagenes/bar3.svg" heading="Earn a Certificate" text="Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a BC, making it over 2000 years old." />
      </div>
      <div className="casa">
        <h1>Our Tracks</h1>
        <p>Lorem Ipsum is simply dummy text of the printing.</p>
      </div>
      <div className="iconos">
        <Icono image="/imagenes/barra.svg" img="/imagenes/estrellita.svg" category="UI/UX Design" title="UI/UX Design for Beginners" price="98" duration="22hr 30min" courses="34 Courses" sales="250 Sales" name="Join Course"></Icono>
        <Icono image="/imagenes/barra1.svg" img="/imagenes/estrellita.svg" category="UI/UX Design" title="UI/UX Design for Beginners" price="98" duration="22hr 30min" courses="34 Courses" sales="250 Sales" name="Join Course"></Icono>
        <Icono image="/imagenes/barra2.svg" img="/imagenes/estrellita.svg" category="UI/UX Design" title="UI/UX Design for Beginners" price="98" duration="22hr 30min" courses="34 Courses" sales="250 Sales" name="Join Course"></Icono>
      </div>
      <div className="fondo2">
        <div className="parte2">
          <div>
            <img src="/imagenes/segunda.svg" alt="" width={500} height={500}></img>
          </div>
          <div >
            <div className="titulo1">
              <h1>Premium<a> Learning</a><br />
                Experience</h1></div>
            <Parte2 buton="/imagenes/sec2.svg" title="Easily Accessible" par="Learning Will fell Very Comfortable With Courslab."></Parte2>
            <Parte2 buton="/imagenes/sec3.svg" title="Fun learning expe" par="Learning Will fell Very Comfortable With Courslab."></Parte2>

          </div>
        </div>
      </div>
      <div className="footer-content"> 
        <Footer  image="imagenes/footer.svg " text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy a type specimen book."></Footer>
        <Footer  title="Company" text="About Us " text1="How to work?" text3="Populer Course" text4="Service"></Footer>
        <Footer title="Courses" text="Service " text1="Ofline Course" text3="Vidio Course"></Footer>
        <Footer title="C" text="FAQ " text1="Help Center" text3="Career" text4="Privacy "></Footer>
        <Footer title="Contac Info" text1="+0913-705-3875" text3="ElizabethJ@jourrapide.com" text4="4808 Skinner Hollow Road
Days Creek, OR 97429"></Footer>
        
       

      </div>
     
    </main >
  );
}
