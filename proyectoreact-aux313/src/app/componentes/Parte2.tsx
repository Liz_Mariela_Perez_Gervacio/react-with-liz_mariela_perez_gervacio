type Parte2Props = {
    par: string,
    buton: any,
    title:any,
}
const XD = (props: Parte2Props) => {
    const { par, buton,title } = props;
    return (
            <div className="pip">
                <button ><img src={buton} alt="" /></button>
                <div className="pop">
                    <h3>{title}</h3>
                <p>{par}</p>
                </div>
            </div>

    )
}
export default XD