type IconoProps = {
  image: any,
  category: any,
  title: any,
  price: any,
  name: any,
  duration: any,
  courses: any,
  sales: any,
  img:any,
}
const Icono = (props: IconoProps) => {
  const { image, category, title, price, name, duration, courses, sales , img} = props;
  return (
    
    <div className="course-card">
      <img className="img" src={image} alt="" width={330} height={300} />
      <div className="course-content">
        <h5 >{category}</h5>
        <div className="btn"><img src={img} /></div>
        <h2 >{title}</h2>
        <p >${price}</p>
        <div className="course-rating">
          <span></span>
        </div>
        <div className="course-info">
          <span>{duration} </span>
          <span>{courses} </span>
          <span>{sales} </span>
        </div>
        <button className="join-course-button">{name}</button>
      </div>
    </div>
    
  )
}

export default Icono
