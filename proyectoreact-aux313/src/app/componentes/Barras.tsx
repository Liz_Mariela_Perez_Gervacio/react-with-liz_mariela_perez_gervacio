
type Barras ={
  icon:any,
  heading:any,
  text:any,
}
const Barras = (props:Barras ) => {
  const{icon,heading,text}=props;
    return (
      <div className="skill-card">
        <div className="icon-container">
          <img src={icon} alt={heading} className="icon" />
        </div>
        <div className="text-container">
          <h2>{heading}</h2>
          <p>{text}</p>
        </div>
      </div>
    );
  };
  
  export default Barras;
