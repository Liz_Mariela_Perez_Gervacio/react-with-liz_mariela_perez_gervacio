import Typography from '@mui/material/Typography';
import * as React from 'react';
import Box from '@mui/material/Box';
import Link from '@mui/material/Link';
import { Directions } from '@mui/icons-material';
import { TextDecoder } from 'util';
type Footer = {
    title:any;
    text:any,
    text1:any,
    text2:any,
    text3:any,
    text4:any,
    image:any,
}

const preventDefault = (props:Footer) =>{
    const  { title,text,text1,text2,text3,text4,image}=props;
    
  return (
    <Box
      sx={{
        display:"flex",
        flexDirection:"column",
        width:"200px",
        margin:"40px 20px",
       
      }}
      
    >
     <Typography sx={{fontWeight:"700"}} variant="h6" gutterBottom>
        {title}
      </Typography>
      <img src={image} ></img>
      <Link sx={{textDecoration:"none", color:"#8A8A8A" , margin:"5px", padding:"10px",marginLeft:"-10px"}} href="#" >
        {text}
      </Link>
      <Link sx={{textDecoration:"none", color:"#8A8A8A", margin:"5px", padding:"10px",marginLeft:"-10px"}}href="#" >
        {text1}
      </Link>
      <Link sx={{textDecoration:"none", color:"#8A8A8A"}}href="#" >
        {text2}
      </Link>
      <Link sx={{textDecoration:"none", color:"#8A8A8A", padding:"10px",marginLeft:"-10px"}}href="#" >
        {text3}
      </Link>
      <Link sx={{textDecoration:"none", color:"#8A8A8A", margin:"5xp", padding:"10px",marginLeft:"-10px"}} href="#" >
        {text4}
      </Link>
    </Box>
  )
}
export default preventDefault



